# WD73C11 firmware source code

I bought a WD73C11 TV and it was amazing.  But the more I looked into it the more I wanted to tinker.  When reading the user manual I noticed that the software licenses included GPL code and I found, "If you are interested in obtaining open source code for this product, please contact Mitsubishi at (800) 332-2119. A nominal handling and mailing charge may apply." so I did!

What they sent me is included here.  I have gathered a number of other related things here as well given the mitsubishi-tv.com/support site is no longer distributing a number of files you might need.

This isn't an extensive collection, but if anyone would like to add files here that the community would like to keep around, I'm game.

Hope you find this helpful!
